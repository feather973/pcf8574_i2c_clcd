#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <sys/ioctl.h>
#include <linux/i2c-dev.h>

int fd;
#define I2C_SLAVE_ADDR		0x27
//#define I2C_SLAVE_FORCE		0x0706

#define RS1_EN1			0x05
#define RS1_EN0			0x01
#define RS0_EN1			0x04
#define RS0_EN0			0x00
#define BACKLIGHT		0x08

void clcd_command_8(unsigned char command)
{
	unsigned char c_buf[2];

	c_buf[0] = (command & 0xF0) | RS0_EN1 | BACKLIGHT;
	c_buf[1] = (command & 0xF0) | RS0_EN0 | BACKLIGHT;

	write(fd, c_buf, 2);
	//usleep(2);
}

void clcd_command(unsigned char command)
{
	unsigned char c_buf[4];

	c_buf[0] = (command & 0xF0) | RS0_EN1 | BACKLIGHT;
	c_buf[1] = (command & 0xF0) | RS0_EN0 | BACKLIGHT;
	c_buf[2] = ((command<<4) & 0xF0) | RS0_EN1 | BACKLIGHT;
	c_buf[3] = ((command<<4) & 0xF0) | RS0_EN0 | BACKLIGHT;
	
	write(fd, c_buf, 4);
	//usleep(2);
}

void clcd_data(unsigned char data)
{
	unsigned char d_buf[4];

	d_buf[0] = (data & 0xF0) | RS1_EN1 | BACKLIGHT;
	d_buf[1] = (data & 0xF0) | RS1_EN0 | BACKLIGHT;
	d_buf[2] = ((data<<4) & 0xF0) | RS1_EN1 | BACKLIGHT;
	d_buf[3] = ((data<<4) & 0xF0) | RS1_EN0 | BACKLIGHT;
	
	write(fd, d_buf, 4);
	//usleep(2);
}

void clcd_goto_XY(unsigned char row, unsigned char col)
{
	unsigned char address = (0x40 * row) + col;
	unsigned char command = 0x80 | address;

	clcd_command(command);
}

void clcd_string(unsigned char row, unsigned char col, char *string)
{
	clcd_goto_XY(row, col);
	while(*string) {
		clcd_data(*string++);
	}
}

void clcd_init(void)
{
	// 0x30
	// delay 5ms
	// 0x30
	// delay 100us
	// 0x30
	// delay 100us
	// 0x20
	// delay 100us
	// 0x28
	// delay 50us
	// 0x08
	// delay 50us
	// 0x01
	// delay 3ms
	// 0x06
	// delay 50us
	// 0x0c
	// delay 50us

	clcd_command_8(0x30);	usleep(5000);
	clcd_command_8(0x30);	usleep(100);
	clcd_command_8(0x30);	usleep(100);
	clcd_command_8(0x20);	usleep(100);

	clcd_command(0x28);		usleep(50);
	clcd_command(0x08);		usleep(50);
	clcd_command(0x01);		usleep(3000);
	clcd_command(0x06);		usleep(50);
	clcd_command(0x0C);		usleep(50);

	printf("Init OK\n");
}


int main(void)
{
	unsigned char buf[100];
	int ret;

	// i2c-1 adapter
	fd = open("/dev/i2c-1", O_RDWR);
	if(fd < 0){
		printf("/dev/i2c-1 adapter open fail\n");
		return -1;
	}
	printf("open success\n");
	
	// force slave addr
//	ret = ioctl(fd, I2C_SLAVE_FORCE, I2C_SLAVE_ADDR);
	ret = ioctl(fd, I2C_SLAVE, I2C_SLAVE_ADDR);
	if(ret < 0){
		printf("i2c-save-test: master trhead: ioctl error\n");
		return -1;
	}
	printf("I2C_SLAVE success\n");

	clcd_init();	
	printf("clcd init success\n");

	char str0[16] = "PCF8574 I2C CLCD";
	char str1[16] = "Raspberry PI 3B+";

	printf("Before string transfer....\n");
	clcd_string(0, 0, str0);
	usleep(5000);		// for stability
	clcd_string(1, 0, str1);
	printf("string transfer complete.....\n");

	close(fd);

	return 0;	
}
